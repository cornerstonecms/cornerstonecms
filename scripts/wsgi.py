from cornerstone.app import create_app

application = create_app('cornerstone.conf')
